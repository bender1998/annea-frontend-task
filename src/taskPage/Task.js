import {Grid, Button, Menu, Typography, MenuItem, CircularProgress, TextField, InputAdornment } from '@mui/material'
import * as XLSX from 'xlsx';
import LineChart from "../components/LineChart";
import i18next from "i18next";
import cookies from 'js-cookie';
import { IoIosArrowDown } from "react-icons/io";
import { MdFileUpload } from "react-icons/md";
import { BsGlobe } from "react-icons/bs"; 
import { useState } from "react";
import { useTranslation } from "react-i18next";
import React, { useEffect, useCallback } from "react";

const Task = () => {

  const { t } = useTranslation();
  const currentLanguageCode = cookies.get('i18next') || 'en';

  const [selectedTimestamp, setSelectedTimestamp] = useState('03');

  const [chartData, setChartData] = useState([]);
  const [availableTimestamps, setAvailableTimestamps] = useState([]);
  const [manageCurrentLanguage, setManageCurrentLanguage] = useState(currentLanguageCode);

  const [anchorEl, setAnchorEl] = useState('');

  const [loading, setLoading] = useState(false);
  const [dateMenu, setDateMenu] = useState(false);

  const languages = [
    {code: 'en', name: 'English'},
    {code: 'pt', name: 'Português'},
    {code: 'de', name: 'Deutsch'}
  ];

  const handleFile = async(e) => {
    setLoading(true);
    const file = e.target.files[0];
    const data = await file.arrayBuffer();
    const workbook = XLSX.readFile(data, {sheetRows: 95, type: 'binary'});
    
    const worksheet = workbook.Sheets[workbook.SheetNames[0]];

    const jsonData = XLSX.utils.sheet_to_json(worksheet, { raw: false });

    setChartData(jsonData);
    setLoading(false);
  };

  const availableDate = useCallback(() => {
    const results = chartData.map(obj => {
      return obj.timestamp.substring(obj.timestamp.indexOf('-') + 4).split(' ')[0];
    });

    const uniqueArray = results.filter(function(item, pos) {
      return results.indexOf(item) === pos;
    });
    setAvailableTimestamps(uniqueArray)
  }, [chartData]);

  useEffect(() => {
    availableDate()
  }, [availableDate]);

  const opensDateSelector = (event) => {
    setDateMenu(true);
    setAnchorEl(event.currentTarget);
  };

  useEffect(() => {
    switch(currentLanguageCode){
      case 'en':
        setManageCurrentLanguage('English');
        break;
      case 'pt':
        setManageCurrentLanguage('Português');
        break;
      case 'de':
        setManageCurrentLanguage('Deutsch');
        break;
      default: break;
    }
  }, [currentLanguageCode]);

  return(
    <Grid>
      <Grid style={{ position: 'absolute', top: 15, right: 25 }}>
        <TextField 
          fullWidth
          variant='outlined'          
          value={manageCurrentLanguage}
          select
          label={t('language_selector')}
          InputProps={{
            startAdornment: (
              <InputAdornment position="start">
                <BsGlobe size={25} style={{ color: '#356a5f' }}/>
              </InputAdornment>
            ),
          }}
        >
        {languages.map(({ code, name }) => (
          <MenuItem value={name} onClick={() => {i18next.changeLanguage(code); setManageCurrentLanguage(name)}}>
            {name}
          </MenuItem>
        ))}
        </TextField>
      </Grid>
      <Grid style={{ display: 'flex', justifyContent: 'center', padding: 25 }}>
        <Typography style={{ fontSize: '2rem' }}>{t('health_index')}</Typography>
      </Grid>
      <Grid style={{ display: 'flex', justifyContent: 'space-between', padding: 15, alignItems: 'center' }}>
        <Button style={{color: loading || !chartData.length ? 'grey' : '#356a5f', borderColor: loading || !chartData.length ? 'grey' : '#356a5f'}}
          variant='outlined'
          endIcon={<IoIosArrowDown/>}
          disabled={loading || !chartData.length}
          onClick={(event) => opensDateSelector(event)}
        >
          {`${t('date_selector')}  ${selectedTimestamp}/2017` }
        </Button>
        <input
          accept="xlsx"
          onChange={(e) => handleFile(e)}
          style={{ display: 'none' }}
          id="raised-button-file"
          multiple
          type="file"
        />
        <label htmlFor="raised-button-file">
        <Button style={{color: loading ? 'grey' : '#356a5f', borderColor: loading ? 'grey' : '#356a5f'}}
            variant='outlined'
            disabled={loading}
            component='span'
            endIcon={loading ? <CircularProgress size={20}/> : <MdFileUpload/>}>
              {t('data_upload')}
          </Button>
        </label> 
      </Grid>
      <Grid style={{ overflowX: 'auto' }}>
        <LineChart chartData={chartData} selectedTimestamp={selectedTimestamp}/>
      </Grid>
      <Grid>
        <Menu style={{ maxHeight: 300 }}
          open={dateMenu}
          onClose={() => setDateMenu(false)}
          anchorEl={anchorEl}
        >
          {availableTimestamps.map((item, index) => {
            return(
              <MenuItem style={{border: selectedTimestamp === item ? '1px solid #356a5f' : '' }}
                onClick={() => setSelectedTimestamp(item)}
                key={index}>
                  {`${item}/2017`}
              </MenuItem>
            )
          })
          }
        </Menu>
      </Grid>
    </Grid>
  );
}

export default Task;