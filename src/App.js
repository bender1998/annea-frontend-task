import './App.css';
import Task from './taskPage/Task'

const App = () => {
  return (
    <div className="App">
      <Task/>
    </div>
  );
}

export default App;
