import React from "react";
import {CategoryScale} from 'chart.js'; 
import Chart from 'chart.js/auto';
import { Line } from 'react-chartjs-2';
import { useTranslation } from "react-i18next";
import { useState } from "react";
import { useEffect } from "react";
import { useCallback } from "react";

const LineChart = (props) => {

  Chart.register(CategoryScale);
  
  const { t } = useTranslation();

  const {chartData, selectedTimestamp} = props;

  const [formatedData, setFormatedData] = useState(chartData);

  const filterTimestamp = useCallback(() => {
    if (!selectedTimestamp.length) {
      setFormatedData(chartData);
      return
    };

    const results = chartData.filter(obj => {
      return obj.timestamp.substring(obj.timestamp.indexOf('-') + 4).split(' ')[0] === selectedTimestamp;
    });

    setFormatedData(results)
    return results
  }, [chartData, selectedTimestamp]);

  useEffect(() => {
    filterTimestamp()
  }, [filterTimestamp]);

  const chartOptions = {
      responsive: true,
      plugins: {
        legend: {
          position: "bottom",
        },
        title: {
          display: true,
        },
      },
  }

  const data = {
    labels: formatedData.map((item) => item.timestamp.substring(item.timestamp.indexOf(' ') + 1, item.timestamp.indexOf(':') + 3)),
    datasets: [{
      label: t('health_index'),
      position: 'bottom',
      data: formatedData.map((item) => parseFloat(item.indicator)),
      backgroundColor: 'rgba(134, 166, 159, 0.3)',
      borderColor: '#356a5f',
      borderWidth: 1,
      fill: true,
      fillColor : '#d7e1df',
      strokeColor : "#202529",
      opacity: 0
    }]
  };

  return(
    <Line height={100} data={data} options={chartOptions}/>
  )
}

export default LineChart;